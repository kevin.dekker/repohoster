# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2019 Olliver Schinagl <oliver@schinagl.nl>

FROM registry.hub.docker.com/library/alpine:latest

LABEL maintainer="Olliver Schinagl <oliver@schinagl.nl>"

EXPOSE 80/tcp

WORKDIR /workdir

HEALTHCHECK CMD wget -q -O "/dev/null" "http://localhost/" || exit 1

RUN \
    apk add --no-cache \
        busybox-extras \
        dumb-init \
        git \
        logrotate \
        openssh-client \
    && \
    rm -rf "/var/cache/apk/"* && \
    addgroup -S -g 82 www-data && \
    addgroup -S httpd && \
    adduser -D -G httpd -H -h "/var/lib/httpd" -S -s "/sbin/nologin" httpd && \
    addgroup httpd www-data && \
    install -D -d -m 644 "/usr/share/webapps/repohoster/" && \
    chown httpd:www-data -R "/usr/share/webapps/repohoster/"

COPY "./bin/index.cgi" "/usr/local/bin/index.cgi"
COPY "./bin/repohoster.sh" "/usr/local/sbin/repohoster.sh"
COPY "./bin/reposync.sh" "/usr/local/bin/reposync.sh"
COPY "./dockerfiles/buildenv_check.sh" "/test/buildenv_check.sh"
COPY "./dockerfiles/docker-entrypoint.sh" "/init"
COPY "./dockerfiles/logrotate.conf" "/etc/logrotate.conf"

RUN chmod go-rwx "/etc/logrotate.conf"

ENTRYPOINT [ "/init" ]
