#!/usr/bin/dumb-init /bin/sh
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2019 Olliver Schinagl <oliver@schinagl.nl>

set -eu

WWW_ROOT="${WWW_ROOT:-/usr/share/webapps/repohoster/}"

e_err()
{
	>&2 echo "ERROR: ${*}"
}

e_warn()
{
	echo "WARN: ${*}"
}

if ! syslogd; then
	e_warn "Failed to start syslog, logging may not work."
fi

sleep 1

if ! httpd -h "${WWW_ROOT:?}" -vv; then
	e_warn "Failed to start httpd server."
fi

if ! mkdir -p "${WWW_ROOT}/cgi-bin/" || \
   [ -e "${WWW_ROOT}/cgi-bin/index.cgi" ] || \
   ! ln -s "/usr/local/bin/index.cgi" "${WWW_ROOT}/cgi-bin/index.cgi"; then
	e_warn "Failed to create 'cgi-bin/index.cgi', listing of files may not work."
fi

for _iface in "/sys/class/net/"*; do
	_iface="${_iface##*/}"
	if [ "${_iface}" = "lo" ]; then
		continue
	fi

	_ip_address="$(ip -o -4 addr show dev "${_iface}" | cut -d ' ' -f 7 | cut -f 1 -d '/' | tail -n 1)"
	echo "Listening on '${_iface}' ${_ip_address:+http://${_ip_address}/}"
done

while sleep 3600; do
	if ! logrotate "/etc/logrotate.conf"; then
		e_warn "Logrotation failed."
	fi
done

exit 0
