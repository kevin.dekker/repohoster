#!/bin/sh
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2019 Olliver Schinagl <oliver@schinagl.nl>

set -eu

DEF_BASE_PATH="/usr/share/webapps/repohoster"


e_err()
{
	>&2 echo "ERROR: ${*}"
}

e_warn()
{
	echo "WARN: ${*}"
}

usage()
{
	echo "Usage: ${0} [OPTIONS] BASE_PATH"
	echo "Clone and/or update a repository and all of its branches into BASE_PATH"
	echo "    -b  Branch to limit syncing too. ex release/v0.1"
	echo "    -h  Print usage"
	echo "    -r  Repository to sync"
	echo
	echo "When not supplied, the default [BASE_PATH] is '${DEF_BASE_PATH}'."
}

cleanup()
{
	trap EXIT

	git credential-cache exit
}

init()
{
	trap cleanup EXIT
	_repo="${1:-}"
	__user="${_repo%:*}"
	_user="${__user##*/}"
	__passwd="${_repo##*${_user}:}"
	_passwd="${__passwd%@*}"
	_host="${__user%${_user}*}${_repo#*@}"

	# Only run on URL's with passwords, passwords that contain // are valid
	if [ "${_repo%%:*}" = "${__user}" ] && \
	   [ "${_passwd#//}" != "${_passwd}" ]; then
		repo="${_repo}"
		return
	fi

	if [ -n "${_repo:-}" ] && [ -n "${_user:-}" ] && [ -n "${_passwd:-}" ]; then
		git config --global credential.helper "cache --timeout=2147483647"
		printf "url=%s\nusername=%s\npassword=%s\n\n" "${_host}" "${_user}" "${_passwd}" | \
		git credential approve
	fi

	# Return password-less URL
	repo="${__user}${_repo:+@}${_repo#*@}"
}

update_workdirs()
{
	_repo="${1:?Missing argument to function}"

	cd "${_repo}"
	eval "$(git for-each-ref \
		    --shell \
		    --format "test -d '${_repo}/%(refname:short)' &&
		              rm -r '${_repo}/%(refname:short)';
		              git worktree add --force \
		                           '${_repo}/%(refname:short)' '%(refname)'" \
		    "refs/heads/" "refs/tags/")"
}

update_workdir()
{
	_base_path="${1:?Missing argument to function}"
	_repo_branches="${2:?Missing argument to function}"
	_repo="${3:?Missing argument to function}"
	_repo_path="${_base_path}/$(basename "${3%%.git}")"

	(
		for _branch in $(echo "${_repo_branches}" | tr -s ' ,;\f\n\t\v' ' '); do
			cd "${_repo_path}"
			_branch_path="${_repo_path}/${_branch}"
			if [ ! -d "${_branch_path}" ]; then
				echo "Not a directory '${_branch}', skipping"
				continue
			else
				git fetch "${_repo}" "${_branch}"
			fi

			cd "${_branch_path}"
			if ! git reset --hard "remotes/origin/${_branch}"; then
				git reset --hard "${_branch}"
			fi
			git clean -d --force -x
		done
	)
}

repo_sync()
{
	_base_path="${1:?Missing argument to function}"
	_repo="${2:-}"
	_repo_path="${_base_path}/$(basename "${_repo%%.git}")"

	(
		if [ -d "${_repo_path}/.git" ]; then
			cd "${_repo_path}/.git"
			if ! git fetch --recurse-submodules --quiet --all; then
				echo "Failed to synchronize repository, continuing"
			fi
		elif [ -n "${_repo:-}" ]; then
			if ! git clone \
			         --bare \
			         --depth 1 \
			         --mirror \
			         --no-single-branch \
			         --recurse-submodules \
			         --shallow-submodules \
			         "${_repo}" "${_repo_path}/.git"; then
				echo "Failed to clone, not a git repository?"
				exit 1
			fi
		fi

		if [ -d "${_repo_path}/.git" ]; then
			update_workdirs "${_repo_path}"
		else
			cd "${_repo_path}"
			for _dir in *; do
				if [ "${_dir}" = "*" ]; then
					_dir="."
				fi
				update_workdirs "${_repo_path}/${_dir}"
			done
		fi
	)
}

main()
{
	while getopts ":b:hr:" _options; do
		case "${_options}" in
		b)
			_branches="${OPTARG},${_branches:-}"
			;;
		h)
			usage
			exit 0
			;;
		r)
			_repo="${OPTARG}"
			if [ -e "${_repo}" ]; then
				_repo="file://${_repo}"
			fi
			;;
		:)
			e_err "Option -${OPTARG} requires an argument."
			exit 1
			;;
		?)
			e_err "Invalid option: -${OPTARG}"
			exit 1
			;;
		esac
	done
	shift "$((OPTIND - 1))"

	base_path="${1:-${BASE_PATH:-${DEF_BASE_PATH}}}"

	init "${_repo:-}"
	if [ -n "${_branches:-}" ]; then
		update_workdir "${base_path}" "${_branches:-}" "${repo:-}"
	else
		repo_sync "${base_path}" "${repo:-}" "${_branches:-}"
	fi
	cleanup
}

main "${@}"

exit 0
