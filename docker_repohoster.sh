#!/bin/sh
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2019 Olliver Schinagl <oliver@schinagl.nl>

set -eu


src_file="$(readlink -f "${0}")"
src_dir="${src_file%%${src_file##*/}}"

while getopts ":r:" _options; do
	case "${_options}" in
	r)
		_repo="${OPTARG}"
		;;
	*) # Pass everything else along
		;;
	esac
done

if [ -e "${_repo:-}" ]; then
	_repo="$(readlink -f "${_repo}")"
	OPT_DOCKER_ARGS="--volume '${_repo}:${_repo}'"
fi

OPT_DOCKER_ARGS="${OPT_DOCKER_ARGS:-}" "${src_dir}/docker_run.sh" "${@}"

exit 0
